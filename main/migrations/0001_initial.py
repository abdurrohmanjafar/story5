# Generated by Django 3.1.2 on 2020-10-16 16:29

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Matkul',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('matkul_name', models.CharField(max_length=30)),
                ('dosen_name', models.CharField(max_length=50)),
                ('jumlah_sks', models.IntegerField()),
                ('tahun_semester', models.IntegerField()),
                ('ruang_kelas', models.CharField(max_length=5)),
            ],
        ),
    ]
