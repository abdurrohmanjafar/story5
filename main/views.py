from django.shortcuts import render,redirect
from django.http import HttpResponseRedirect
from .models import Matkul
from .forms import inputform


def tambah(request):
    form = inputform()
    return render(request, 'add.html', {'form' : form})

def jadwal(request):
    matkuls = Matkul.objects.all()
    context = {
        'matkul' : matkuls
    }
    return render(request, 'result.html', context)

def form(request):
    form = inputform(request.POST or None)
    if(form.is_valid() and request.method == "POST"):
        form.save()
        return  HttpResponseRedirect('/jadwal')
    else:
        return HttpResponseRedirect('/')

def description(request, id):
    try:
        matkuls = Matkul.objects.get(id=id)
        context = {
            'matkul' : matkuls
            }
        return render(request,'detail.html', context)
    except Exception:
        return HttpResponseRedirect('/jadwal')

def delete(request, id):
    try:
        Matkul.objects.get(id=id).delete()
        return HttpResponseRedirect('/jadwal')
    except Exception:
        return HttpResponseRedirect('/jadwal')



