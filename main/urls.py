from django.urls import path
from . import views

app_name = 'main'

urlpatterns = [
    path('', views.tambah, name='tambah'),
    path('jadwal/', views.jadwal, name='jadwal'),
    path('jadwal/<int:id>/', views.description, name='deskripsi'),
    path('jadwal/<int:id>/delete/', views.delete, name='delete'),
    path('tambah/', views.form, name='form'),

]
